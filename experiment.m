% A program to test a Sobel, Log and Canny edge detection on a set
% of grayscale images.

METHODS = ['sobel'; 'log  '; 'canny';];
METHODS = cellstr(METHODS);

% Threshold parameters.
T_PARAMS = [0.01, 0.025, 0.05, 0.075, 0.1];

% Smoothing parameters.
SIG_PARAMS = [0.5, 1.0, 1.5, 2.0, 2.5];

% Threshold width parameters.
WIDTH_PARAMS = [0.2, 0.4, 0.6];

% KEYSs relating to threshold and smoothing combinations.
KEYS = {'a', 'b', 'c', 'd','e'};

% Set of image files.
myFiles = dir( 'raw_images/*.png' );

% Iterate through each image.
for k = 1:length(myFiles)
  [pathstr,fileName,ext] = fileparts(myFiles(k).name);
  fprintf(1, 'Processing %s\n', strcat(fileName,ext));
  fullPath = strcat('raw_images/',fileName,ext);
  % Iterate through each method.
  for m = 1:3
      type = METHODS(m);
      
      % Iterate through each threshold value.
      for i = 1:length(T_PARAMS)
          thresh = T_PARAMS(i);
          
          % Iterate through each smoothing value.
          for j = 1:length(SIG_PARAMS)
            sig = SIG_PARAMS(i);
            
			for w = 1:length(WIDTH_PARAMS)
				% Ensure the input image is grayscale.
				rgbImage = imread(fullPath);
				grayImage = rgb2gray(rgbImage);
				
				% If the method is Sobel then we don't use sigma.
				if m == 1
					edgeImage = edge(grayImage, char(type), thresh);
					outputFileName = strcat(fileName,'_',type,'_', KEYS(i), '_', '.png');
				% If the method is Log then we can simply use the raw threshold value.
				elseif m == 2
					edgeImage = edge(grayImage, char(type), thresh, sig);
					outputFileName = strcat(fileName,'_',type,'_', KEYS(i), '_', KEYS(j), '.png');
				else
					lowerThresh = WIDTH_PARAMS(w);
					edgeImage = edge(grayImage, char(type), [lowerThresh, thresh], sig);
					outputFileName = strcat(fileName,'_',type,'_', KEYS(i), '_', KEYS(j), '_', KEYS(w), '.png');
				end
				
				% Highlight edges in red.
				edgeImage = edgeImage * 255;
				edgeImage = cast(cat(3, edgeImage, zeros(size(edgeImage)), zeros(size(edgeImage))), class(edgeImage));

				% Blend the images together.
				blendedImage = imfuse(rgbImage, edgeImage,'blend');
				
				% Write out the raw edges image.
				imwrite(edgeImage, char(strcat('output_images/raw_edges/', outputFileName)));
				
				% Write out the blended image.
				imwrite(blendedImage, char(strcat('output_images/blended/', outputFileName)));
				
				% Only repeat the width loop for the Canny method.
				if m != 3
					break
				end
			end
			% Skip the rest of the sigma loop for the Sobel method.
			if m == 1
				break
			end
          end
      end
  end
end

fprintf(1, 'Experiment completed successfully.\n');